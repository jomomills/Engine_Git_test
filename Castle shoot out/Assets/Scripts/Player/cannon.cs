﻿using UnityEngine;
using System.Collections;

public class cannon : MonoBehaviour 
{
    public Rigidbody cannonball;
    private float counter = 0;

    void Start()
    {

    }

    void Update()
    {
        counter += 0.01f;
        if (Input.GetKey(KeyCode.E) && counter >= 2)
        {
            Rigidbody instantiatedcannonball = Instantiate(cannonball, transform.position, transform.rotation) as Rigidbody;
            instantiatedcannonball.velocity = transform.TransformDirection(new Vector3(0, 0, -80));
            instantiatedcannonball.name = "cannonball";
            counter = 0;
        }
    }
}