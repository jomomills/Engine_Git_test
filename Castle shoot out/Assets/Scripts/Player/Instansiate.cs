﻿using UnityEngine;
using System.Collections;

public class Instansiate : MonoBehaviour 
{

	public float myRotationSpeed = 1.0f;
	
	void Start () 
	{
	
	}

	void Update () 
	{
		if (Input.GetKey(KeyCode.D))
		{
			transform.Rotate(0, myRotationSpeed * Time.deltaTime * 1, 0);
		}
		if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(0, myRotationSpeed * Time.deltaTime * -1, 0);
		}
	
	}
}
