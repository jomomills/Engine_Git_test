﻿using UnityEngine;
using System.Collections;

public class Barrelmove : MonoBehaviour 
{
    public float RotationSpeed = 1.0f;

	void Start () 
    {
	
	}
	
	void Update ()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime * -1, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime * 1, 0);
        }
	}
}
