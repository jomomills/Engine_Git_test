﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour
{

    public float myRotationSpeed = 1.0f;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, myRotationSpeed * Time.deltaTime * 1, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, myRotationSpeed * Time.deltaTime * -1, 0);
        }

    }
}