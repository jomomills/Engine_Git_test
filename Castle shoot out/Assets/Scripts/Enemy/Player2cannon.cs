﻿using UnityEngine;
using System.Collections;

public class Player2cannon : MonoBehaviour
{
    public Rigidbody Cannonball;
    private float counter = 0;

    void Start()
    {

    }

    void Update()
    {
        counter += 0.01f;
        if (Input.GetKey(KeyCode.RightShift) && counter >= 2)
        {
            Rigidbody instantiatedCannonball = Instantiate(Cannonball, transform.position, transform.rotation) as Rigidbody;
            instantiatedCannonball.velocity = transform.TransformDirection(new Vector3(0, 0, -80));
            instantiatedCannonball.name = "Cannonball";
            counter = 0;
        }
    }
}