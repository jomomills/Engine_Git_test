﻿using UnityEngine;
using System.Collections;

public class Player2Health : MonoBehaviour
{
    private GameObject sm;

    void Start()
    {
        sm = GameObject.Find("Text");
    }

    void Update()
    {

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Cannonball")
        {
            Debug.Log("triggering");
            sm.GetComponent<scoreManager>().score += 10;
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}