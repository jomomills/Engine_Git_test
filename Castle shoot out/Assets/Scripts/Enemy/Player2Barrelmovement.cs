﻿using UnityEngine;
using System.Collections;

public class Player2Barrelmovement : MonoBehaviour
{
    public float RotationSpeed = 1.0f;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime * -1, 0);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime * 1, 0);
        }
    }
}
